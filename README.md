## Your Lab Environment

You first need to configure your lab. You require access to three host running Linux.

| Role      | Hostname | IP        |
| --------- | -------- | --------- |
| etcd node | etcd1    | 10.0.0.10 |
| etcd node | etcd2    | 10.0.0.20 |
| etcd node | etcd3    | 10.0.0.30 |

Make sure your network settings are configured correctly.

![](images/lab-env.png)

- Attach the first network adapter as NAT to access the internet from your guest
- Attach a second network adapter as Host-only adapter for connections between the host and virtual machines ([VMware Workstation Player](https://docs.vmware.com/en/VMware-Workstation-Player-for-Linux/16.0/com.vmware.player.linux.using.doc/GUID-93BDF7F1-D2E4-42CE-80EA-4E305337D2FC.html) / [VirtualBox](https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/network_hostonly.html)). This creates a virtual network adapter

## Introduction

[etcd](https://etcd.io/) is a distributed key-value store relied on by many platforms and tools, including [Kubernetes](https://kubernetes.io/), [Vulcand](https://vulcand.github.io/), and [Doorman](https://github.com/youtube/doorman). Within Kubernetes, etcd is used as a global configuration store that stores the state of the cluster.

At the end of this tutorial, you will have a secure 3-node etcd cluster running on your servers.

## Step 1 - Installing etcd on the nodes

In this step. we will install `etcd` on all nodes. `etcd` and its client `etcdctl` are available as binaries, which we’ll download, extract, and move to a directory that’s part of the `PATH` environment variable.

First, check the [pre-built release binaries](https://github.com/etcd-io/etcd/releases) before you proceed to get the latest release. At the moment, the latest release for our architecture is etcd-v**3.5.4**-linux-amd64.tar.gz.

1. Make sure `curl` and `wget` are installed on your nodes. 


```bash
sudo apt update
sudo apt install -y wget curl
```

2. Download the latest release of `etcd`

```bash
wget https://github.com/etcd-io/etcd/releases/download/v3.5.4/etcd-v3.5.4-linux-amd64.tar.gz
```

3. Extract downloaded archive file

```bash
tar xvf etcd-v3.5.4-linux-amd64.tar.gz
```

4. Change to new directory

```bash
cd etcd-v3.5.4-linux-amd64
```

5. Move `etcd` and `etcdctl` binary files to `/usr/local/bin`

```bash
sudo mv etcd etcdctl etcdutl /usr/local/bin 
```

6. Confirm the version and check if the binaries are executable

```bash
$ etcd --version
etcd Version: 3.5.4
Git SHA: 08407ff76
Go Version: go1.16.15
Go OS/Arch: linux/amd64
```

```bash
$ etcdctl version
etcdctl version: 3.5.4
API version: 3.5
```

```bash
$ etcdutl version
etcdutl version: 3.5.4
API version: 3.5
```

7. Create `etcd` configuration file directory.

```bash
sudo mkdir /etc/etcd
```

8. Create `etcd` configuration data directory.

```bash
sudo mkdir -p /var/lib/etcd/
```

9. Create `etcd` system user

```bash
sudo groupadd --system etcd
sudo useradd -s /sbin/nologin --system -g etcd etcd
```

10. Set `/var/lib/etcd/` directory ownership to `etcd` user. This allows the `etcd` binary to write data to this directory with restricted rights.

```bash
sudo chown -R etcd:etcd /var/lib/etcd/
```

## Step 2 - Creating a Systemd Unit File for etcd

The quickest way to run `etcd` may appear to be used directly with the command `etcd`. However, this will not work because it will make `etcd` run as a foreground process. We need to run the `etcd` binary as a background **service** instead.

Ubuntu 20.04 uses systemd as its init system, which means we can create new services by writing unit files and placing them inside the `/etc/systemd/system/` directory.

1. Using your editor, create a new file named `etcd.service` within that directory `/etc/systemd/system/`

```bash
sudo nano /etc/systemd/system/etcd.service
```

2. Next, copy the following code block into the `etcd.service` file:

```bash
[Unit]
Description=etcd distributed reliable key-value store
Documentation=https://github.com/etcd-io/etcd
After=network.target

[Service]
User=etcd
Type=notify
Environment=ETCD_DATA_DIR=/var/lib/etcd
Environment=ETCD_NAME=%m
ExecStart=/usr/local/bin/etcd
Restart=always
RestartSec=10s
LimitNOFILE=40000

[Install]
WantedBy=multi-user.target
```

Close and save the `etcd.service` file by pressing `CTRL+X` followed by `Y`.

This unit file defines a service that runs the executable at `/usr/local/bin/etcd`, notifies systemd when it has finished initializing, and always restarts if it ever exits.

**Note:** If you’d like to understand more about systemd and unit files, or want to tailor the unit file to your needs, Digitalocean wrote an excellent [Understanding Systemd Units and Unit Files](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files) guide.

3. Reload systemd service and start `etcd`

```bash
sudo systemctl daemon-reload
sudo systemctl start etcd.service
```

4. Enable the service to start at system boot

```bash
$ sudo systemctl enable etcd.service
Created symlink /etc/systemd/system/multi-user.target.wants/etcd.service → /etc/systemd/system/etcd.service.
```

5. Check the service state

```bash
$ systemctl status etcd.service
● etcd.service - etcd distributed reliable key-value store
     Loaded: loaded (/etc/systemd/system/etcd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-05-23 04:38:39 UTC; 45s ago
```



## Step 3 - Testing etcd

Before we continue with clustiering, let’s first understand what etcd can do in terms of functionality. In this step, we are going to manually send requests to etcd to add, retrieve, update, and delete data from it.

By default, etcd exposes an API that listens on port `2379` for client communication. This means we can send raw API requests to etcd using an HTTP client. However, it’s quicker to use the official etcd client `etcdctl`, which allows you to create/update, retrieve, and delete key-value pairs using the `put`, `get`, and `del` subcommands, respectively.

Make sure you’re still inside the **etcd1** node, and run the following `etcdctl` commands to confirm your etcd installation is working.

First, create a new entry using the `put` subcommand.

The `put` subcommand has the following syntax:

```bash
etcdctl put key value
```

On **etcd1**, run the following command:

```bash
etcdctl put foo bar
```

The command you just ran instructs etcd to write the value "bar" to the key foo in the store.

You will then find **OK** printed in the output, which indicates the data persisted:

We can then retrieve this entry using the `get` subcommand, which has the syntax `etcdctl get key`:

```bash
etcdctl get foo
```

You will find this output, which shows the key on the first line and the value you inserted earlier on the second line:

```bash
$ etcdctl get foo
foo
bar
```

We can delete the entry using the `del` subcommand, which has the syntax `etcdctl del key`:

```bash
$ etcdctl del foo
1
```

You will find the output **1**, which indicates the number of entries deleted.

Now, let’s run the `get` subcommand once more in an attempt to retrieve a deleted key-value pair:

```bash
etcdctl get foo
```

You will not receive an output, which means `etcdctl` is unable to retrieve the key-value pair. This confirms that after the entry is deleted, it can no longer be retrieved.

In this step, we used the `etcdctl` client to send requests to etcd. At this point, we are running three separate instances of etcd, each acting independently from each other. However, etcd is designed as a distributed key-value store, which means multiple etcd instances can group up to form a single *cluster*; each instance then becomes a *member* of the cluster. After forming a cluster, you would be able to retrieve a key-value pair that was inserted from a different member of the cluster. In the next step, we will transform our 3 single-node clusters into a single 3-node cluster.

## Step 4 - Forming a Cluster Using Static Discovery

To create one 3-node cluster instead of three 1-node clusters, we must configure these etcd installations to communicate with each other. This means each one must know the IP addresses of the others. This process is called *discovery*. Discovery can be done using either *static configuration* or *dynamic service discovery*. In this step, we will discuss the difference between the two, as well as update our playbook to set up an etcd cluster using static discovery.

Discovery by static configuration is the method that requires the least setup; this is where the endpoints of each member are passed into the `etcd` command before it is executed. To use static configuration, the following conditions must be met prior to the initialization of the cluster:

- the number of members are known
- the endpoints of each member are known
- the IP addresses for all endpoints are static

If these conditions cannot be met, then you can use a dynamic discovery service. With dynamic service discovery, all instances would register with the discovery service, which allows each member to retrieve information about the location of other members.

Since we know we want a 3-node etcd cluster, and all our servers have static IP addresses, we will use static discovery.

**etcd1** node `/etc/etcd/etcd.conf.yml`:

```bash
name: etcd1
data-dir: /var/lib/etcd
initial-advertise-peer-urls: http://10.0.0.10:2380
listen-peer-urls: http://10.0.0.10:2380,http://127.0.0.1:2380
advertise-client-urls: http://10.0.0.10:2379
listen-client-urls: http://10.0.0.10:2379,http://127.0.0.1:2379
initial-cluster-state: new
initial-cluster: etcd1=http://10.0.0.10:2380,etcd2=http://10.0.0.20:2380,etcd3=http://10.0.0.30:2380
```

**etcd2** node `/etc/etcd/etcd.conf.yml`:

```bash
name: etcd2
data-dir: /var/lib/etcd
initial-advertise-peer-urls: http://10.0.0.20:2380
listen-peer-urls: http://10.0.0.20:2380,http://127.0.0.1:2380
advertise-client-urls: http://10.0.0.20:2379
listen-client-urls: http://10.0.0.20:2379,http://127.0.0.1:2379
initial-cluster-state: new
initial-cluster: etcd1=http://10.0.0.10:2380,etcd2=http://10.0.0.20:2380,etcd3=http://10.0.0.30:2380
```

**etcd3** node `/etc/etcd/etcd.conf.yml`:

```bash
name: etcd3
data-dir: /var/lib/etcd
initial-advertise-peer-urls: http://10.0.0.30:2380
listen-peer-urls: http://10.0.0.30:2380,http://127.0.0.1:2380
advertise-client-urls: http://10.0.0.30:2379
listen-client-urls: http://10.0.0.30:2379,http://127.0.0.1:2379
initial-cluster-state: new
initial-cluster: etcd1=http://10.0.0.10:2380,etcd2=http://10.0.0.20:2380,etcd3=http://10.0.0.30:2380
```



Here’s a brief explanation of each parameter:

- `name` - a human-readable name for the member. By default, etcd uses a unique, randomly-generated ID to identify each member; however, a human-readable name allows us to reference it more easily inside configuration files and on the command line. Here, we will use the hostnames as the member names (i.e., `etcd1`, `etcd2`, and `etcd3`).
- `initial-advertise-peer-urls` - a list of IP address/port combinations that other members can use to communicate with this member. In addition to the API port (`2379`), etcd also exposes port `2380` for peer communication between etcd members, which allows them to send messages to each other and exchange data. Note that these URLs must be reachable by its peers (and not be a local IP address).
- `listen-peer-urls` - a list of IP address/port combinations where the current member will listen for communication from other members. This must include all the URLs from the `--initial-advertise-peer-urls` flag, but also local URLs like `127.0.0.1:2380`. The destination IP address/port of incoming peer messages must match one of the URLs listed here.
- `advertise-client-urls` - a list of IP address/port combinations that clients should use to communicate with this member. These URLs must be reachable by the client (and not be a local address). If the client is accessing the cluster over public internet, this must be a public IP address.
- `listen-client-urls` - a list of IP address/port combinations where the current member will listen for communication from clients. This must include all the URLs from the `--advertise-client-urls` flag, but also local URLs like `127.0.0.1:2379`. The destination IP address/port of incoming client messages must match one of the URLs listed here.
- `initial-cluster` - a list of endpoints for each member of the cluster. Each endpoint must match one of the corresponding member’s `initial-advertise-peer-urls` URLs.
- `initial-cluster-state` - either `new` or `existing`.

To ensure consistency, etcd can only make decisions when a majority of the nodes are healthy. This is known as establishing *quorum*. In other words, in a three-member cluster, quorum is reached if two or more of the members are healthy.

If the `initial-cluster-state` parameter is set to `new`, `etcd` will know that this is a new cluster being bootstrapped, and will allow members to start in parallel, without waiting for quorum to be reached. More concretely, after the first member is started, it will not have quorum because one third (33.33%) is less than or equal to 50%. Normally, etcd will halt and refuse to commit any more actions and the cluster will never be formed. However, with `initial-cluster-state` set to `new`, it will ignore the initial lack of quorum.

If set to `existing`, the member will try to join an existing cluster, and expects quorum to already be established.

1. Using your editor, create the config files on all 3 nodes.
   ```bash
   sudo nano /etc/etcd/etcd.conf.yml
   ```

2. Update your systemd init configuration to use this newly created config file
   ```bash
   sudo nano /etc/systemd/system/etcd.service
   ```

3. Copy the following code block into the `etcd.service` file
   ```bash
   [Unit]
   Description=etcd distributed reliable key-value store
   Documentation=https://github.com/etcd-io/etcd
   After=network.target
   
   [Service]
   User=etcd
   Type=notify
   ExecStart=/usr/local/bin/etcd --config-file /etc/etcd/etcd.conf.yml
   Restart=always
   RestartSec=10s
   LimitNOFILE=40000
   
   [Install]
   WantedBy=multi-user.target
   ```

   

4. **IMPORTANT**: To form the new three-member cluster, you must first stop the `etcd` service and clear the data directory before launching the cluster.

   ```bash
   sudo systemctl daemon-reload
   sudo systemctl stop etcd.service
   sudo rm -rf /var/lib/etcd/*
   ```

5. Start etcd with updated configuration. You need at least two nodes ready, execute on all nodes within 1-2mins.

   ```bash
   sudo systemctl start etcd.service
   ```

6. Run `etcdctl endpoint health --cluster` to check the cluster health:
   ```bash
   etcdctl endpoint health --cluster
   ```

7. To confirm that the `etcd` cluster is actually working, we can, once again, create an entry on one member node, and retrieve it from another member node:
   ```bash
   etcd1:$ etcdctl put message "created on etcd1"
   ```

8. Now retrieve the same entry using the key `message`:
   ```bash
   etcd2:$ etcdctl get message
   ```

   
