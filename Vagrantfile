# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "bento/ubuntu-20.04"

  config.vm.provider :virtualbox do |v|
    v.memory = 2048
    v.cpus = 1
    v.linked_clone = true
  end

  # Disable the new default behavior introduced in Vagrant 1.7, to
  # ensure that all Vagrant machines will use the same SSH key pair.
  # See https://github.com/hashicorp/vagrant/issues/5005
  config.ssh.insert_key = false

  boxes = [
    { :name => "etcd1", :ip => "10.0.0.10" },
    { :name => "etcd2", :ip => "10.0.0.20" },
    { :name => "etcd3", :ip => "10.0.0.30" }

  ]

  ansible_groups = {
  "etcd" => [
    "etcd1","etcd2","etcd3"
    ]
  }

  # Provision each of the VMs.
  boxes.each_with_index do |opts, index|
    config.vm.define opts[:name] do |config|
      config.vm.hostname = opts[:name]
      config.vm.network :private_network, ip: opts[:ip]
      # Autodetect internal network addresses and autoconfigure hosts
      #config.vm.provision :hosts, :sync_hosts => true

      # Provision all the VMs in parallel using Ansible after last VM is up.
      if index == boxes.size-1
        config.vm.provision "ansible" do |ansible|
          ansible.compatibility_mode = "2.0"
          ansible.playbook = "provision.yml"
          ansible.limit = "all"
          ansible.become = true
          ansible.groups = ansible_groups
        end
      end
    end
  end

end
