# 02 Time-Series-Database

## Your Lab Environment

This lab we are running in Docker on your host. Part of this setup is your existing etcd lab running in a virtual environment.

| Role              | Hostname / Image | IP        |
| ----------------- | ---------------- | --------- |
| etcd node         | etcd1            | 10.0.0.10 |
| etcd node         | etcd2            | 10.0.0.20 |
| etcd node         | etcd3            | 10.0.0.30 |
| prometheus docker | prometheus       | -         |

## Prerequisites

Before following this tutorial make sure you have:

- Docker Desktop
- Linux kernel update package
- Enabled Windows Subsystem for Linux
- Installed Ubuntu 20.04 from the Windows Microsoft Store

## Introduction

[Prometheus](https://prometheus.io/) is a powerful, open-source monitoring system that collects metrics from your services and stores them in a **time-series database**. It offers a multi-dimensional data model, a flexible query language, and diverse visualization possibilities through tools like [Grafana](https://grafana.com/).

By default, Prometheus only exports metrics about itself (e.g. the number of requests it’s received, its memory consumption, etc.). But, you can greatly expand Prometheus by installing *exporters*, optional programs that generate additional metrics.

Exporters - both the official ones that the Prometheus team maintains as well as the community-contributed ones - provide information about everything from infrastructure, databases, and web servers to messaging systems, APIs, and more.

## Step 1 — Installing Prometheus

This section will explain how to install the main Prometheus server using Docker. The Prometheus server is the central piece of the Prometheus ecosystem and is responsible for collecting and storing metrics as well as processing expression queries and generating alerts.

Docker container images for all Prometheus components are hosted under the **[prom](https://hub.docker.com/u/prom/)** organization on Docker Hub. Running the `prom/prometheus` Docker image without any further options starts the Prometheus server with an example configuration file located at `/etc/prometheus/prometheus.yml` inside the container. It also uses a Docker data volume mounted at `/prometheus` inside the container to store collected metrics data. This data volume directory is actually a directory on the host which Docker auto-creates when the container is first started. The data inside it is persisted between restarts of the same container.

There are multiple ways for overriding the default configuration file. For example, a custom configuration file may be passed into the container from the host filesystem as a Docker data volume, or you could choose to build a derived Docker container with your own configuration file baked into the container image. We will pass a configuration file from the host filesystem for this exercise.

First, start your **WSL2 terminal** running Ubuntu 20.04 on your notebook. This may vary depending on your setup.

Create a minimal Prometheus configuration file on the host filesystem at `~/prometheus.yml`:

```bash
nano ~/prometheus.yml
```

Add the following contents to the file

```bash
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:

  - job_name: 'prometheus'

    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 5s

    static_configs:
      - targets: ['localhost:9090']
```

This example configuration makes Prometheus scrape metrics from itself (since Prometheus also exposes metrics about itself in a Prometheus-compatible format)

Start the Prometheus Docker container with the external configuration file:

```bash
docker run -d -p 9090:9090 -v ~/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```

This command is quite long and contains many command line options. Let’s take a look at it in more detail:

- The `-d` option starts the Prometheus container in detached mode, meaning that the container will be started in the background and will not be terminated by pressing `CTRL+C`.
- The `-p 9090:9090` option exposes Prometheus’s web port (9090) and makes it reachable via the external IP address of the host system.
- The `-v [...]` option mounts the `prometheus.yml` configuration file from the host filesystem into the location within the container where Prometheus expects it (`/etc/prometheus/prometheus.yml`).

You can list all running Docker containers using the following command:

```bash
docker ps
```

For example, you will see something similar to the following for the Prometheus Docker container:

```bash
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS          PORTS                                       NAMES
44a1720c1af3   prom/prometheus   "/bin/prometheus --c…"   53 seconds ago   Up 52 seconds   0.0.0.0:9090->9090/tcp, :::9090->9090/tcp   peaceful_roentgen
```

Using the container ID shown in the `docker ps` output, you may inspect the logs of the running Prometheus server with the command `docker logs <container_id>`:

```bash
docker logs 44a1720c1af3
```

As with many other commands in Linux, docker only requires the unique part of the container id. The command `docker logs 44a1720c1af3` can also be written like `docker logs 44a`

To find out where on the host’s filesystem the metrics storage volume is stored, you can run the following with your container_id:

```bash
docker inspect 44a1720c1af3
```

Find a section in the output that looks similar to this:

```bash
"Mounts": [
    {
        "Type": "volume",
        "Name": "df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb",
        "Source": "/var/lib/docker/volumes/df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb/_data",
        "Destination": "/prometheus",
        "Driver": "local",
        "Mode": "",
        "RW": true,
        "Propagation": ""
    },
    {
        "Type": "bind",
        "Source": "/home/dominic/prometheus.yml",
        "Destination": "/etc/prometheus/prometheus.yml",
        "Mode": "",
        "RW": true,
        "Propagation": "rprivate"
    }
],
```

In this example, the metrics are stored in `/var/lib/docker/volumes/df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb/_data` on the host system. This directory was automatically created by Docker when first starting the Prometheus container. It is mapped into the `/prometheus` directory in the container. Data in this directory is persisted across restarts of the same container.

You should now be able to reach your Prometheus server at `http://0.0.0.0:9090/`. Verify that it is collecting metrics about itself by heading to `http://0.0.0.0:9090/status` 

Check the **Status / Targets** section. The **State** column for your metrics endpoint should show the the target’s state as **UP**.

![](images/prometheus-targets.png)

## Step 2 - Using the expression browser

Let us explore data that Prometheus has collected about itself. To use Prometheus's built-in expression browser, navigate to http://0.0.0.0:9090/graph and choose the "Table" view within the "Graph" tab.

As you can gather from `0.0.0.0:9090/metrics`, one metric that Prometheus exports about itself is named `prometheus_target_interval_length_seconds` (the actual amount of time between target scrapes). 

Enter this in the expression console:

```bash
prometheus_target_interval_length_seconds
```

This should return a number of different time series (along with the latest value recorded for each), each with the metric name `prometheus_target_interval_length_seconds`, but with different labels. These labels designate different latency percentiles and target group intervals.

![](images/prometheus_target_interval_length_seconds.png)

If we are interested only in 99th percentile latencies, we could use this query:

```bash
prometheus_target_interval_length_seconds{quantile="0.99"}
```

![](images/prometheus-quantile.png)

To count the number of returned time series, you could write:

```bash
count(prometheus_target_interval_length_seconds)
```

![](images/prometheus-count.png)

To graph expressions, use the "Graph" tab. For example, enter the following expression to graph the per-second rate of chunks being created in the self-scraped Prometheus:

```bash
rate(prometheus_tsdb_head_chunks_created_total[1m])
```

![](images/prometheus-rate.png)

## Step 3 - Setting up Node Exporter

In this section, we will install the Prometheus Node Exporter. The Node Exporter is a server that exposes Prometheus metrics about the host machine (node) it is running on. This includes metrics about the machine’s filesystems, networking devices, processor usage, memory usage, and more.

Note that running the Node Exporter on Docker poses some challenges since its entire purpose is to expose metrics about the host it is running on. If we run it on Docker without further options, Docker’s namespacing of resources such as the filesystem and the network devices will cause it to only export metrics about the container’s environment, which will differ from the host’s environment. Thus it is usually recommended to run the Node Exporter directly on the host system outside of Docker.

This part of the lab you will try to figure out on your own. The [Node Exporter guide](https://prometheus.io/docs/guides/node-exporter/) gives you a good starting point.

The steps required involve:

- Login to your `etcd1` host

- Download an run `node-exporter`, expose `/metrics` endpoint on `etcd1` 

- Configure your `~/prometheus.yml` on your notebook so it additionally scrapes your `node-exporter` on `etcd1`
  ```bash
  scrape_configs:
  - job_name: node
    static_configs:
    - targets: ['10.0.0.10:9100']
  ```

- Restart your docker container with `docker restart <container_id>`:
  ```bash
  docker restart 44a1720c1af3
  ```

- Check if your new targets are available. (Note: The state should be **UP**)
  ![](images/prometheus-nodeexporter.png)

- Explore the newly added metrics using the Prometheus UI. Navigate to http://0.0.0.0:9090/graph in your browser and use the main expression bar to create some useful graphs.

## Step 4 - Study diary

The goal of this exercise was to give you an insight into time-series databases, starting with hands-on first. After getting a feeling of how you install and use it, you should be able to research some questions and note it down in your study diary.

- What are the pros and cons of [time-series databases](https://en.wikipedia.org/wiki/Time_series_database)? How are they different from key-value stores and relational databases?
- Prometheus is only one popular product. Research others, choose 2-3 other products and describe what they are used for.
- Prometheus is using a pull method to gather it's data (Prometheus connecting to `/metrics` API endpoint). Other systems like Graphite use a push method. Which one is better? 
