[MariaDB](https://mariadb.org/) is an open-source relational database management system, commonly used as an alternative for MySQL as the database portion of the popular LAMP (Linux, Apache, MySQL, PHP/Python/Perl) stack. It is intended to be a drop-in replacement for MySQL.

## Docker Compose

Docker simplifies the process of managing application processes in containers. While containers are similar to virtual machines in certain ways, they are more lightweight and resource-friendly. This allows developers to break down an application environment into multiple isolated services.

For applications depending on several services, orchestrating all the containers to start up, communicate, and shut down together can quickly become unwieldy. Docker Compose is a tool that allows you to run multi-container application environments based on definitions set in a YAML file. It uses service definitions to build fully customizable environments with multiple containers that can share networks and data volumes.

In this guide, you’ll demonstrate how to install Docker Compose on an Ubuntu 20.04 server and how to get started using this tool.

## Step 1 - Installing Docker Compose

To make sure you obtain the most updated stable version of Docker Compose, you’ll download this software from its [official Github repository](https://github.com/docker/compose).

First, confirm the latest version available in their [releases page](https://github.com/docker/compose/releases). At the time of this writing, the most current stable version is `2.6.0`.

The following command will download the `2.6.0` release and save the executable file at `/usr/local/bin/docker-compose`, which will make this software globally accessible as `docker-compose`:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose
```

Set the correct permissions so that the `docker-compose` command is executable:

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

To verify that the installation was successful, you can run:

```bash
docker-compose --version
```

## Step 2 - Run MariaDB

Before installing WordPress with Docker you will need to have somewhere to store the data. Start off by making a new directory where you wish to store the files for WordPress and MariaDB for example in your home directory.

```bash
mkdir ~/wordpress && cd ~/wordpress
```

For the last tutorial, we mainly used YAML files to configure the services we were running in Docker. This time, we use environment variables, as they have some security benefits over static files.

The simplest way to pass an environment variable with Docker run is with the `-e` flag. We can check all available flags for [MariaDB on Dockerhub](https://hub.docker.com/_/mariadb). Let's start with the simplest form with only a password:

```bash
docker run -e MARIADB_ROOT_PASSWORD=secret-pw mariadb:latest
```

We can exit from the container by pressing `Ctrl + C`

Create a `docker-compose.yml` file:

```bash
nano docker-compose.yml
```

Insert the following content in your `docker-compose.yml` file:

```yaml
version: "3.9"
    
services:
  db:
    image: mariadb
    environment:
      MARIADB_ROOT_PASSWORD: secret-pw
```

The `docker-compose.yml` file typically starts off with the `version` definition. This will tell Docker Compose which configuration version you’re using.

You then have the `services` block, where you set up the services that are part of this environment. In your case, you have a single service called `db`. This service uses the `mariadb` image.

With the `docker-compose.yml` file in place, you can now execute Docker Compose to bring your environment up. The following command will download the necessary Docker images, create a container for the `db` service, and run the containerized environment in background mode:

```bash
docker-compose up -d
```

Docker Compose will first look for the defined image on your local system, and if it can’t locate the image it will download the image from Docker Hub.

Your environment is now up and running in the background. To verify that the container is active, you can run:

```bash
docker-compose ps
```

You’ve seen how to set up a `docker-compose.yml` file and bring your environment up with `docker-compose up`. You’ll now see how to use Docker Compose commands to manage and interact with your containerized environment.

To check the logs produced by your MariaDB container, you can use the `logs` command:

```bash
docker-compose logs
```

The `stop` command will terminate the container execution, but it won’t destroy any data associated with your containers:

```bash
docker-compose stop
```

If you want to remove the containers, networks, and volumes associated with this containerized environment, use the `down` command:

```bash
docker-compose down
```

## Step 3 - Run Wordpress

We need to add Wordpress into our `docker-compose.yml` . https://docs.docker.com/storage/volumes/)

Update your existing `docker-compose.yml` and and add the wordpress service.

```yaml
version: '3.1'

services:

  db:
    image: mariadb
    volumes:
      - db_data:/var/lib/mysql
    restart: unless-stopped
    environment:
      MARIADB_ROOT_PASSWORD: rootpress
      MARIADB_DATABASE: wordpress
      MARIADB_USER: wordpress
      MARIADB_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    volumes:
      - wordpress_data:/var/www/html
    ports:
      - '8000:80'
    restart: always
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress

volumes:
  db_data: {}
  wordpress_data: {}
```

The docker volumes `db_data` and `wordpress_data` persists updates made by WordPress to the database, as well as the installed themes and plugins. [Learn more about docker volumes](

### Shutdown and cleanup

The command [`docker-compose down`](https://docs.docker.com/compose/reference/down/) removes the containers and default network, but preserves your WordPress database.

The command `docker-compose down --volumes` removes the containers, default network, and the WordPress database.

## Next steps

- [ ] You have no access to the database from your host. You can solve this with an interactive shell (google it) or with a database management tool like [Adminer](https://hub.docker.com/_/adminer/). Update your`docker-compose.yml` with a adminer service and link them together. Read the Dockerhub reference to help you.
  ```yml
  adminer:
      image: adminer
      ports:
        - 8080:8080
  ```

- [ ] When you finished setting up Wordpress, you should create a backup.
