#!/bin/bash

[[ ! -d encryption ]] && mkdir -p encryption
[[ ! -f encryption/keyfile ]] && echo -n "1;"$(openssl rand -hex 32) > encryption/keyfile
[[ ! -f encryption/keyfile.key ]] && openssl rand -hex 128 > encryption/keyfile.key
[[ ! -f encryption/keyfile.enc ]] && openssl enc -aes-256-cbc -md sha1 -pass file:encryption/keyfile.key -in encryption/keyfile -out encryption/keyfile.enc
[[ -f encryption/keyfile.enc ]] && rm -f encryption/keyfile