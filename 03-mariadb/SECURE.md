[[_TOC_]]

## How to secure a database

Ideas to secure and harden your wordpress installation

- [ ] [Restricting Database User Privileges](https://wordpress.org/support/article/hardening-wordpress/#restricting-database-user-privileges)

  For normal WordPress operations, such as posting blog posts, uploading media files, posting comments, creating new WordPress users and installing WordPress plugins, the MySQL database user only needs data read and data write privileges to the MySQL database; `SELECT`, `INSERT`, `UPDATE` and `DELETE`.

  Therefore any other database structure and administration privileges, such as `DROP`, `ALTER` and `GRANT` can be revoked.

- [ ] **[Data Backups](https://wordpress.org/support/article/hardening-wordpress/#data-backups)**

  Back up your data regularly, including your MySQL databases.

  Data integrity is critical for trusted backups. Encrypting the backup, keeping an independent record of MD5 hashes for each backup file, and/or placing backups on read-only media increases your confidence that your data has not been tampered with.

- [ ] **[Data-at-Rest Encryption](https://mariadb.com/kb/en/data-at-rest-encryption-overview/)**
  At-rest encryption means the data-at-rest like data files and logs are encrypted on the disk, makes it almost impossible for someone to access or steal a hard disk and get access to the original data.

- [ ] **[Securing Connections for Client and Server](https://mariadb.com/kb/en/securing-connections-for-client-and-server/)**
  Data can be exposed to risks both in transit and at rest and requires protection in both states. In-transit encryption protects your data if communications are intercepted while data moves between hosts through network, either from your site and the cloud provider, between services or between clients and the server.

  In order to secure communications with the MariaDB Server using TLS, you need to create a private key and an X509 certificate for the server. You may also want to create additional private keys and X509 certificates for any clients that need to connect to the server with TLS. [This guide](https://mariadb.com/kb/en/certificate-creation-with-openssl/) covers how to create a private key and a self-signed X509 certificate with OpenSSL.

## Restricting Database User Privileges



## Data-at-Rest Encryption

In my case, I called my database `wordpress` and on initialization, named the user in Wordpress `test`. With this command I can output the file content of the `wp_users` table in the `wordpress` database. 

```bash
docker-compose exec db cat /var/lib/mysql/wordpress/wp_users.ibd | strings
```

The output is human-readable, therefor we know that those files are not encrypted.

### Encryption Key Management

MariaDB provides three encryption key management solutions, so how you databases manage encryption key depends on the solution you are using. 

In order to encrypt your tables with encryption keys using the File Key Management plugin, you first need to create the file that contains the encryption keys. The file needs to contain two pieces of information for each encryption key. First, each encryption key needs to be identified with a 32-bit integer as the key identifier. Second, the encryption key itself needs to be provided in hex-encoded form. These two pieces of information need to be separated by a semicolon. For example, the file is formatted in the following way:

```bash
<encryption_key_id1>;<hex-encoded_encryption_key1>
<encryption_key_id2>;<hex-encoded_encryption_key2>
```

To append a new encryption keys to a new key file, you could execute the following:

```bash
echo -n "1;"$(openssl rand -hex 32) > keyfile
```

The new key file would look something like the following after this step:

```bash
1;664949e96be22c77afbf1f50c620caa9b02fe31410d1a0e82a44a2e5bfb427f6
```

The key identifiers give you a way to reference the encryption keys from MariaDB. In the example above, you could reference these encryption keys using the key identifiers `1`, `2` or `100` with the `ENCRYPTION_KEY_ID` table option or with system variables such as `innodb_default_encryption_key_id`.

You can generate a random encryption password using the `openssl rand` command. For example, to create a random 256 character encryption password, you could execute the following:

```
openssl rand -hex 128 > encryption/keyfile.key
```

You can encrypt the key file using the `openssl enc` command. For example, to encrypt the key file with the encryption password created in the previous step, you could execute the following:

```
openssl enc -aes-256-cbc -md sha1 \
   -pass file:encryption/keyfile.key \
   -in encryption/keyfile \
   -out encryption/keyfile.enc
```

Running this command reads the unencrypted `keyfile` file created above and creates a new encrypted `keyfile.enc` file, using the encryption password stored in `keyfile.key`. Once you've finished preparing your system, you can delete the unencrypted `keyfile` file, as it's no longer necessary.

### Setup MariaDB config

Although the plugin's shared library is distributed with MariaDB by default, the plugin is not actually installed by MariaDB by default. We can enable it with the configuration

```bash
[mariadb]
...
plugin_load_add = file_key_management
```

Configuring the Path to an Encrypted Key File

```bash
[mariadb]
...
loose_file_key_management_filename = /etc/mysql/encryption/keyfile.enc
loose_file_key_management_filekey = FILE:/etc/mysql/encryption/keyfile.key
```

The File Key Management plugin currently supports two encryption algorithms for encrypting data: `AES_CBC` and `AES_CTR`. Both of these algorithms use [Advanced Encryption Standard (AES)](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) in different modes. AES uses 128-bit blocks, and supports 128-bit, 192-bit, and 256-bit keys. 

The recommended algorithm is `AES_CTR`

```bash
[mariadb]
...
loose_file_key_management_encryption_algorithm = AES_CTR
```

