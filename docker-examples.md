# Docker examples

## Limit Docker Container CPU Usage

Just like RAM usage, Docker containers don’t have any default limitations for the host’s CPU. Giving containers unlimited CPU usage can lead to issues.

For example, if you have a host with 2 CPUs and want to give a container access to one of them, use the option --cpus="1.0". The command for running an Ubuntu container with access to 1 CPU would be:

```
sudo docker run -it --cpus="1.0" ubuntu
```

## Set Maximum Memory Access 

To limit the maximum amount of memory usage for a container, add the --memory option to the docker run command. Alternatively, you can use the shortcut -m.

Within the command, specify how much memory you want to dedicate to that specific container.

The command should follow the syntax:

```bash
sudo docker run -it --memory="1g" ubuntu
```

Example for Prometheus

```bash
docker run -d -p 9090:9090 -v ~/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml --cpus="1.0" --memory="1g" --name prometheus prom/prometheus
```



## Access resource consumption

Get bash interactive shell for a running container

```bash
docker exec -it prometheus sh
```

Access `pcuacct` and `memory` control groups wich are responsible for tracking and limiting the CPU and memory consumption.

```bash
cat /sys/fs/cgroup/memory/memory.usage_in_bytes
cat /sys/fs/cgroup/cpuacct/cpuacct.usage
```

